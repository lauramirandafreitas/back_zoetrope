const sequelize = require("..config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
    media_id:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    content:{
        type: DataTypes.STRING,
        allowNull: false
    },

    id:{
        type: DataTypes.STRING,
        allowNull: false
    },

    user_id:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    review:{
        type: DataTypes.STRING,
        allowNull: false
    },

    like:{
        type: DataTypes.STRING,
        allowNull: true
    },

});

Comment.associate = function(models) {
    Comment.hasOne(models.media);
    Comment.hasOne(models.user);
}

module.export = Comment;
