const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

//Definição da model com atributos
const User = sequelize.define('User',{
    id:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    age:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    password:{
        type: DataTypes.STRING,
        allowNull: false
    },

    adress:{
        type: DataTypes.STRING,
        allowNull: false
    },

});

User.associate = function(models) {
   User.hasMany(models.comment,{as:"comments",foreignKey:"commentId"});

}

module.exports = User;