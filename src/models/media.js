const sequelize = require("..config/sequelize");
const DataTypes = require("sequelize");

const Media = sequelize.define('Media', {
    
    id:{
        type: DataTypes.STRING,
        allowNull: false
    },

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },

});

Media.associate = function(models) {
    Media.hasMany(models.Comment);
}

module.export = Media;
