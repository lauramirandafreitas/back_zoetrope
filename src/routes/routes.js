const { Router } = require('express');
const UserController = require('../controllers/controllersUser');
const router = Router ();

router.get ('/user', UserController.index);
router.get ('/user/:id', UserController.show);
router.post ('/user', UserController.create);
router.put ('/user/:id', UserController.update);
router.delete ('/user/:id', UserController.destroy);

module.exports = router;


