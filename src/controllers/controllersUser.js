const { response } = require ('express');
const User = require ('../models/User');

// create
const create = (req,res) => {
    try {
        const user = await User.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
    }catch(err){
        res.status(500).json ({error: err});

    }
};

// read
const index = async(req,res) => {
    try {
        const users = await User.findAll ();
        return res.status(200).json({users});
    }catch(err) {
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

// update
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(update) {
            const user = await User.findByPk(id);
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }

};


// delete
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deletedUser = await User.destroy ({where: {id: id}});
        if(deletedUser) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

// export
module.exports = {
    index,
    show,
    create,
    update,
    destroy
};

// relacionamentos add
const addRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const role = await Role.findByPk(req.body.RoleId);
        await user.setRole(role);
        return res.status(200).jason(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

// relacionamentos remove
const removeRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        await user.setRole(null);
        return res.status(200).jason(user);
    }catch(err){
        return res.status(500).json({err});
    }
};





    
